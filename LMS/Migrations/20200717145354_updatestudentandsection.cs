﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class updatestudentandsection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Result",
                table: "Students");

            migrationBuilder.RenameColumn(
                name: "Total_Attendance",
                table: "Students",
                newName: "phone");

            migrationBuilder.AddColumn<string>(
                name: "Section_Name_Arabic",
                table: "Sections",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Section_Name_Arabic",
                table: "Sections");

            migrationBuilder.RenameColumn(
                name: "phone",
                table: "Students",
                newName: "Total_Attendance");

            migrationBuilder.AddColumn<string>(
                name: "Result",
                table: "Students",
                nullable: true);
        }
    }
}
