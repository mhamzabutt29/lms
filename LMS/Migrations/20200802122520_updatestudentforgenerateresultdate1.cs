﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class updatestudentforgenerateresultdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Result_Generate_Date",
                table: "Students");

            migrationBuilder.AddColumn<string>(
                name: "Result_Date",
                table: "Subjects",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Result_Date",
                table: "Subjects");

            migrationBuilder.AddColumn<string>(
                name: "Result_Generate_Date",
                table: "Students",
                nullable: true);
        }
    }
}
