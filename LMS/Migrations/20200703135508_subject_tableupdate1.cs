﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class subject_tableupdate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Subjects",
                newName: "Subject_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Subject_Id",
                table: "Subjects",
                newName: "Id");
        }
    }
}
