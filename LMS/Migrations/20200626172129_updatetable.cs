﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class updatetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "age",
                table: "Students",
                newName: "Age");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Students",
                newName: "Promoted_To");

            migrationBuilder.AddColumn<string>(
                name: "DOB",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Date_Of_Admission",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Iqama_Number",
                table: "Students",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name_Arabic",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name_English",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Nationality_Arabic",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Nationality_English",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "POB",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Passport_Number",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Previous_School_Arabic",
                table: "Students",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Previous_School_English",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Result",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<float>(
                name: "Total_Attendance",
                table: "Students",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.CreateTable(
                name: "Grades",
                columns: table => new
                {
                    Grade_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Grade_Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Grades", x => x.Grade_Id);
                });

            migrationBuilder.CreateTable(
                name: "Sections",
                columns: table => new
                {
                    Section_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Section_Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sections", x => x.Section_Id);
                });

            migrationBuilder.CreateTable(
                name: "Semesters",
                columns: table => new
                {
                    Semester_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Semester_number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Semesters", x => x.Semester_Id);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Student_Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Quran = table.Column<float>(nullable: false),
                    Islamic_Studies = table.Column<float>(nullable: false),
                    Arabic = table.Column<float>(nullable: false),
                    English = table.Column<float>(nullable: false),
                    Maths = table.Column<float>(nullable: false),
                    Science = table.Column<float>(nullable: false),
                    Social = table.Column<float>(nullable: false),
                    Computer = table.Column<float>(nullable: false),
                    Arts = table.Column<float>(nullable: false),
                    Conduct = table.Column<float>(nullable: false),
                    Attendance = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Student_Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Grades");

            migrationBuilder.DropTable(
                name: "Sections");

            migrationBuilder.DropTable(
                name: "Semesters");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.DropColumn(
                name: "DOB",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Date_Of_Admission",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Iqama_Number",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Name_Arabic",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Name_English",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Nationality_Arabic",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Nationality_English",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "POB",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Passport_Number",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Previous_School_Arabic",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Previous_School_English",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Result",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Total_Attendance",
                table: "Students");

            migrationBuilder.RenameColumn(
                name: "Age",
                table: "Students",
                newName: "age");

            migrationBuilder.RenameColumn(
                name: "Promoted_To",
                table: "Students",
                newName: "Name");
        }
    }
}
