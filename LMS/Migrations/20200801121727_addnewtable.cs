﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class addnewtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PCS",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Roll = table.Column<int>(nullable: false),
                    Name_English = table.Column<string>(maxLength: 50, nullable: false),
                    Name_Arabic = table.Column<string>(nullable: false),
                    Gender = table.Column<string>(nullable: false),
                    Nationality_English = table.Column<string>(nullable: false),
                    Nationality_Arabic = table.Column<string>(nullable: false),
                    DOB = table.Column<string>(nullable: false),
                    POB = table.Column<string>(nullable: false),
                    Passport_Number = table.Column<string>(nullable: false),
                    Iqama_Number = table.Column<string>(nullable: false),
                    Date_Of_Admission = table.Column<string>(nullable: false),
                    Previous_School_English = table.Column<string>(maxLength: 50, nullable: false),
                    Previous_School_Arabic = table.Column<string>(maxLength: 50, nullable: false),
                    Grade_Name = table.Column<string>(nullable: false),
                    Grade_Name_Arabic = table.Column<string>(nullable: false),
                    phone = table.Column<string>(nullable: false),
                    Promoted_To = table.Column<string>(nullable: true),
                    IsActive = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PCS", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PCS");
        }
    }
}
