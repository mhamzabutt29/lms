﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMS.Migrations
{
    public partial class update_subject_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Student_Id",
                table: "Subjects",
                newName: "Id");

            migrationBuilder.AddColumn<string>(
                name: "Class",
                table: "Subjects",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Grade",
                table: "Subjects",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Semester",
                table: "Subjects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Student_Roll",
                table: "Subjects",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Class",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "Grade",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "Semester",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "Student_Roll",
                table: "Subjects");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Subjects",
                newName: "Student_Id");
        }
    }
}
