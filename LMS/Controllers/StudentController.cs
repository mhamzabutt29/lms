﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Rotativa.AspNetCore;
using LMS.Models;
using LMS.Helping_classes;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using System.Text;
using System.Net.Http.Headers;

namespace LMS.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StudentController : Controller
    {
        private Data.DataContext _db;
        private readonly IHostingEnvironment _hostingEnvironment;
        public StudentController(Data.DataContext db, IHostingEnvironment hostingEnvironment)
        {
            _db = db;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public IActionResult Add(student s)
        {

            var s_id = _db.Students.FirstOrDefault(x => x.Roll == s.Roll);
            if (s_id != null)
            {
                return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} already exist", s.Roll) });
            }
            else
            {
                if (s.Grade_Name != "Please Select" && s.Grade_Name_Arabic!= "Please Select")
                {
                    Complete_Result c = new Complete_Result();
                    var d = s.Date_Of_Admission.Split("-");
                    s.Date_Of_Admission = d[2]+"-"+c.convert_month_to_alphabets(d[1])+"-"+d[0];
                    d = s.DOB.Split("-");
                    s.DOB = d[2] + "-" + c.convert_month_to_alphabets(d[1]) + "-" + d[0];
                    s.IsActive = "Yes";
                    _db.Students.Add(s);
                    _db.SaveChanges();
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student Added {0}", s.Roll) });
                }
                else
                {
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Please Select Valid Grade", s.Roll) });
                }
            }
        }




        public IActionResult Edit(student s)
        {
            Models.student student = _db.Students.FirstOrDefault(x => x.Roll == s.Roll);
            if (student != null)
            {
                Complete_Result c = new Complete_Result();
                var dob = student.DOB.Split("-");
                var d = dob[2]+"-"+ c.convert_month_alphabets_to_number(dob[1])+"-"+c.convert_day_single_to_double_digit(dob[0]);
                student.DOB = d;
                var doa = student.Date_Of_Admission.Split("-");
                d = doa[2] + "-" + c.convert_month_alphabets_to_number(doa[1]) + "-" + c.convert_day_single_to_double_digit(doa[0]);
                student.Date_Of_Admission = d;
                var grades = _db.Grades.ToList();
                ViewBag.grades = grades;
                var country = _db.Countries.ToList();
                ViewBag.country = country;
                return View("./Views/Student/Edit_student.cshtml", student);
            }
            return View();
        }


        public IActionResult Details(student s)
        {
            Models.student student = _db.Students.FirstOrDefault(x => x.Roll == s.Roll);
            if (student != null)
            {
                var d = student.Date_Of_Admission.Split("-");
                var month = d[1];
                Complete_Result c = new Complete_Result();
                var t = c.convert_month_to_alphabets(month);
                student.Date_Of_Admission = d[0] + "-" + t + "-" + d[2];
                d = student.DOB.Split("-");
                month = d[1];
                var m = c.convert_month_to_alphabets(month);
                student.DOB = d[0] + "-" + m + "-" + d[2];
                ViewBag.student = student;
                return View("./Views/Student/Detail_Student.cshtml", new Models.Subject());
            }
            return View();
        }

        public IActionResult Delete(string id)
        {
            var std = _db.Students.FirstOrDefault(x => x.Roll == int.Parse(id));
            if (std != null)
            {
                var std1 = std;
                std1.IsActive = "No";
                _db.Students.Remove(std);
                _db.Students.Update(std1);
                _db.SaveChanges();
                var marks = _db.Subjects.Where(x => x.Student_Roll == int.Parse(id)).ToList();
                foreach (var m in marks)
                {
                    m.IsActive = "No";
                }
                var marks1 = marks;
                _db.Subjects.RemoveRange(marks);
                _db.Subjects.UpdateRange(marks1);
                _db.SaveChanges();
                return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} Deleted", int.Parse(id)) });
            }
            else
                return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} Not available", int.Parse(id)) });
        }
        [HttpPost]
        public IActionResult Update_student(student s)
        {

            var std = _db.Students.FirstOrDefault(x => x.Roll == s.Roll);
            Complete_Result c = new Complete_Result();
            var d = s.Date_Of_Admission.Split("-");
            s.Date_Of_Admission = d[2] + "-" + c.convert_month_to_alphabets(d[1]) + "-" + d[0];
            d = s.DOB.Split("-");
            s.DOB = d[2] + "-" + c.convert_month_to_alphabets(d[1]) + "-" + d[0];

            _db.Students.Remove(std);
            var tmp = _db.PCS.Where(x => x.Roll == s.Roll).ToList();
            foreach(var t in tmp)
            {
                var g_e = t.Grade_Name;
                var g_a = t.Grade_Name_Arabic;
                Models.Previous_Classes_of_Students pcs = new Previous_Classes_of_Students{
                    Date_Of_Admission=s.Date_Of_Admission,
                    DOB=s.DOB,
                    Gender=s.Gender,
                    Name_Arabic=s.Name_Arabic,
                    Name_English=s.Name_English,
                    Iqama_Number=s.Iqama_Number,
                    Passport_Number=s.Passport_Number,
                    phone=s.phone,
                    POB=s.POB,
                    Previous_School_Arabic=s.Previous_School_Arabic,
                    Previous_School_English=s.Previous_School_English,
                    Grade_Name=g_e,
                    Grade_Name_Arabic=g_a,
                    Nationality_Arabic=s.Nationality_Arabic,
                    Nationality_English=s.Nationality_English,
                    IsActive=s.IsActive,
                    Roll=s.Roll,
                };
                _db.PCS.Remove(t);
                _db.PCS.Update(pcs);
                _db.SaveChanges();

            }
            _db.Students.Update(s);
            _db.SaveChanges();

            return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} Updated", s.Roll) });

        }
        public IActionResult Active_Student(string id)
        {
            var std = _db.Students.FirstOrDefault(x => x.Roll == int.Parse(id));
            std.IsActive = "Yes";
            var subject = _db.Subjects.Where(x => x.Student_Roll == int.Parse(id)).ToList();
            foreach(var s in subject)
            {
                s.IsActive = "Yes";
            }
            var std1 = std;
            var subject1 = subject;
            _db.Students.Remove(std);
            _db.Subjects.RemoveRange(subject);
            _db.Students.Update(std1);
            _db.Subjects.UpdateRange(subject1);
            _db.SaveChanges();
            return Ok();
        }
        public IActionResult view_dashboard(string message)
        {
            var students = _db.Students.Where(x => x.IsActive == "Yes").OrderBy(x => x.Grade_Name).ToList();
            ViewBag.student = students;
            var c_g_n = _db.Grades.ToList();
            ViewBag.grade = c_g_n;
            ViewBag.message = message;
            var country = _db.Countries.ToList();
            ViewBag.country = country;
            var student_inActive = _db.Students.Where(x => x.IsActive == "No").OrderBy(x => x.Grade_Name).ToList();
            if (student_inActive.Any())
                ViewBag.student1 = student_inActive;
            else
                ViewBag.student1 = null;
            return View("./Views/Dashboard.cshtml", new Models.student());
        }


        [HttpPost]
        public IActionResult Add_subject_Marks(Subject s)
        {
            var temp1 = _db.Subjects.FirstOrDefault(x => x.Student_Roll == s.Student_Roll && x.Semester == 1 && s.Class==x.Class);
            var temp2 = _db.Subjects.FirstOrDefault(x => x.Student_Roll == s.Student_Roll && x.Semester == 2 && s.Class == x.Class);
            if (s.Semester == 1)
            {
                if (temp1 == null)
                {
                    var r_date = s.Result_Date.Split("-");
                    s.Result_Date = r_date[2] + "-" + r_date[1] + "-" + r_date[0];
                    s.IsActive = "Yes";
                    _db.Subjects.Add(s);
                    _db.SaveChanges();
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student Updated {0}", s.Student_Roll) });
                }
                else
                {
                    var r_date = s.Result_Date.Split("-");
                    s.Result_Date = r_date[2] + "-" + r_date[1] + "-" + r_date[0];
                    var t = s;
                    t.IsActive = "Yes";
                    //t.Result_Date = temp1.Result_Date;
                    _db.Subjects.Remove(temp1);
                    _db.Subjects.Update(t);
                    _db.SaveChanges();
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student Updated {0}", s.Student_Roll) });
                }
            }
            else
            {
                if (temp2 == null)
                {
                    var r_date = s.Result_Date.Split("-");
                    s.Result_Date = r_date[2] + "-" + r_date[1] + "-" + r_date[0];
                    s.IsActive = "Yes";
                    _db.Subjects.Add(s);
                    _db.SaveChanges();
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student Updated {0}", s.Student_Roll) });
                }
                else
                {
                    var r_date = s.Result_Date.Split("-");
                    s.Result_Date = r_date[2] + "-" + r_date[1] + "-" + r_date[0];
                    var t = s;
                    t.IsActive = "Yes";
                    //t.Result_Date = temp2.Result_Date;
                    _db.Subjects.Remove(temp2);
                    _db.Subjects.Update(t);
                    _db.SaveChanges();
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student Updated {0}", s.Student_Roll) });
                }
            }
           
        }
        [HttpPost]
        public JsonResult Edit_Marks(string id, string semester, string grade)
        {
            var sem = _db.Subjects.Where(x => x.Class == grade && x.Semester == int.Parse(semester) && x.Student_Roll == int.Parse(id));
            if(sem != null)
            {
                return Json(sem);
            }
            else { return Json(1); }
        }
        [HttpPost]
        public IActionResult Edit_subject_Marks(Subject sub)
        {
            var s = _db.Subjects.FirstOrDefault(x => x.Student_Roll == sub.Student_Roll && x.Class == sub.Class && sub.Semester == x.Semester);
            sub.IsActive = s.IsActive;
            sub.Result_Date = s.Result_Date;
            _db.Subjects.RemoveRange(s);
            _db.Subjects.Update(sub);
            _db.SaveChanges();
            return RedirectToAction("view_dashboard", "Student", new { message = "Student Marks updated"});
        
        }
        public IActionResult Result(student s)
        {
            Models.student student = _db.Students.FirstOrDefault(x => x.Roll == s.Roll);
            if (student != null)
            {
                int marks = 0;
                var std = _db.Subjects.Where(x => x.Student_Roll == s.Roll && x.Class==s.Grade_Name);
                if (std.Any()) { 
                    Total total = new Total();
                    Complete_Result complete_Result = new Complete_Result();
                    //student.Date_Of_Admission = complete_Result.convert_date(student.Date_Of_Admission);
                    //student.DOB = complete_Result.convert_date(student.DOB);
                    ViewData["marks"] = std.FirstOrDefault(x => x.Semester == 1);
                    ViewBag.gender = complete_Result.gender(student.Gender);
                    ViewBag.a_promoted = complete_Result.promoted_to(student.Grade_Name);
                    ViewBag.e_promoted = complete_Result.promoted_to(student.Grade_Name, "a");
                    if (std.Count() > 1)
                    {
                        var sem1 = std.FirstOrDefault(x => x.Semester == 1);
                        ViewData["marks1"] = std.FirstOrDefault(x => x.Semester == 2);
                        var sem2 = std.FirstOrDefault(x => x.Semester == 2);
                        ViewBag.result_generated_date = complete_Result.convert_date(sem2.Result_Date);
                        marks = 100;
                        total = complete_Result.Total(sem1, sem2);
                        var r_d = sem2.Result_Date;
                        int ses = int.Parse(r_d.Substring(r_d.Length - 4));
                        ViewBag.session = ses;
                        ViewBag.session1 = ses - 1;
                        ViewBag.islamic_session = ses - 578;
                        ViewBag.islamic_session1 = ses - 579;
                        ViewBag.num = std.FirstOrDefault(x => x.Semester == 1).Class;
                    }
                    else if (std.Count() == 1)
                    {
                        var sem = std.FirstOrDefault(x => x.Semester == 1);
                        if (sem == null)
                        {
                            sem = std.FirstOrDefault(x => x.Semester == 2);
                        }
                        marks = 50;
                        total = complete_Result.Semester1(sem);
                        ViewBag.result_generated_date = complete_Result.convert_date(sem.Result_Date);
                        
                        var arabic_grade2 = complete_Result.return_arabic_grade(s.Grade_Name);
                        ViewBag.arabic_grade = arabic_grade2;

                        if (std.FirstOrDefault(x => x.Semester == 1) != null)
                        {
                            ViewBag.heading_a = "تقرير الفصل الدراسي الأول" + " " + arabic_grade2;
                            ViewBag.heading_e = "First Semester Report " + sem.Class;
                            ViewBag.sem_e = "1st Semester";
                            ViewBag.sem_a = "الفصل الدراسي الأول";
                        }
                        else
                        {
                            ViewBag.heading_a = "تقرير الفصل الدراسي الثاني" + " " + arabic_grade2;
                            ViewBag.heading_e = "Second Semester Report " + sem.Class;
                            ViewBag.sem_e = "2nd Semester";
                            ViewBag.sem_a = "الفصل الدراسي الثاني";
                        }
                        ViewData["total"] = total;
                        var r_d = sem.Result_Date;
                        int ses = int.Parse(r_d.Substring(r_d.Length - 4));
                        ViewBag.session = ses;
                        ViewBag.session1 = ses - 1;
                        ViewBag.islamic_session = ses - 578;
                        ViewBag.islamic_session1 = ses - 579;
                    }
                    ViewData["grade_letter"] = complete_Result.alphabet_grades(total, marks);
                    double cgpa = complete_Result.calculate_gpa(total, marks);
                    cgpa = Math.Round(cgpa, 1);

                    ViewData["total"] = total;
                    ViewData["cgpa"] = cgpa;
                    ViewData["credits"] = complete_Result.get_credits(total, marks);
                    ViewData["grade_letter"] = complete_Result.alphabet_grades(total, marks);
                    var average = complete_Result.average(total.Total_Marks);
                    ViewBag.average = average;
                    var arabic_grade1 = complete_Result.return_arabic_grade(s.Grade_Name);
                    ViewBag.arabic_grade = arabic_grade1;
                    if (std.Count() == 1)
                    {
                        //return View("./Views/Result_Card_Front.cshtml", student);
                        return new Rotativa.AspNetCore.ViewAsPdf("./Views/Result_Card_Front.cshtml", student, viewData: ViewData)
                        {

                            PageMargins = { Left = 7, Bottom = 0, Right = 3, Top = 5 },
                            PageSize = Rotativa.AspNetCore.Options.Size.A4,
                            FileName = "Result for " + student.Name_English + "(" + student.Grade_Name + ")" + ".pdf"
                        };
                    }
                    else
                    {
                        //return View("./Views/Result_Front.cshtml", student);

                        return new Rotativa.AspNetCore.ViewAsPdf("./Views/Result_Front.cshtml", student, viewData: ViewData)
                        {

                            PageMargins = { Left = 7, Bottom = 0, Right = 3, Top = 0 },
                            PageSize = Rotativa.AspNetCore.Options.Size.A4,
                            FileName = "Result for " + student.Name_English + "(" + student.Grade_Name + ")" + ".pdf"
                        };
                    }

                }
                else
                {
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} result not available", student.Roll) });
                }
            }
            return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} result not available", student.Roll) });
        }
        public IActionResult Print_Previous_Result()
        {
            var c_g_n = _db.Grades.ToList();
            ViewBag.grade = c_g_n;
            var std = _db.PCS.ToList();
            return View("./Views/Add_Result.cshtml", std);
        }

        public JsonResult CheckDb(string data)
        {
            var std = _db.Students.FirstOrDefault(x => x.Roll.ToString() == data);
            if (std != null)
            {
                return Json(1);
            }
            return Json(0);
        }
        [HttpPost]
        public IActionResult filter_std()
        {
            var grade = Request.Form["filter_std"];
            var iqama = Request.Form["iqama"];
            var passport = Request.Form["passport"];
            var roll = Request.Form["roll"];
            if (!String.IsNullOrEmpty(grade))
            {
                var students = _db.Students.Where(x => x.Grade_Name == grade && x.IsActive=="Yes").ToList();
                ViewBag.student = students;
            }
            else if (!String.IsNullOrEmpty(iqama))
            {
                var students = _db.Students.Where(x => x.Iqama_Number == iqama && x.IsActive == "Yes").ToList();
                ViewBag.student = students;
            }
            else if (!String.IsNullOrEmpty(passport))
            {
                var students = _db.Students.Where(x => x.Passport_Number == passport && x.IsActive == "Yes").ToList();
                ViewBag.student = students;
            }
            else if (!String.IsNullOrEmpty(roll.ToString()))
            {
                int roll1 = int.Parse(roll);
                var students = _db.Students.Where(x => x.Roll == roll1 && x.IsActive == "Yes").ToList();
                ViewBag.student = students;
            }
            else
            {
                var students = _db.Students.Where(x=>x.IsActive == "Yes").ToList();
                ViewBag.student = students;
            }
            var c_g_n = _db.Grades.ToList();
            ViewBag.grade = c_g_n;
            var country = _db.Countries.ToList();
            ViewBag.country = country;
            var student_inActive = _db.Students.Where(x => x.IsActive == "No").OrderBy(x => x.Grade_Name).ToList();
            if (student_inActive.Any())
                ViewBag.student1 = student_inActive;
            else
                ViewBag.student1 = null;
            return View("./Views/Dashboard.cshtml", new Models.student());

        }


        //public async Task<IActionResult> Read_excel(IList<IFormFile> files1)
        
        //{

            //IFormFile files = Request.Form.Files[0];

            //string folderName = "files";
            //string webRootPath = _hostingEnvironment.WebRootPath;
            //string newPath = Path.Combine(webRootPath, folderName);
            //if (!Directory.Exists(newPath))
            //{
            //    Directory.CreateDirectory(newPath);
            //}
            //if (files.Length > 0)
            //{
            //    string sFileExtension = Path.GetExtension(files.FileName).ToLower();
            //    string fullPath = Path.Combine(newPath, files.FileName);
            //    using (var stream = new FileStream(fullPath, FileMode.Create))
            //    {
            //        await files.CopyToAsync(stream);
            //    }
            //}

            //if (files.FileName.Contains("Students"))
            //{
            //    Get_Excel_File f = new Get_Excel_File(_db, _hostingEnvironment);
            //    f.read_excel();
            //}
            //else if (files.FileName.Contains("Semester1") || files.FileName.Contains("Semester2"))
            //{
            //    Add_Students_Marks f = new Add_Students_Marks(_db, _hostingEnvironment);
            //    f.read_excel(files.FileName);
            //}
            //else
            //{
            //    return Ok();
            //}


            //return Ok("1");
            
       // }
        public void add_bulk_students()
        {
            Get_Excel_File f = new Get_Excel_File(_db, _hostingEnvironment);
            f.read_excel();
        }
        public void add_sem1_results()
        {
            Add_Students_Marks f = new Add_Students_Marks(_db, _hostingEnvironment);
            f.read_excel();
        }
        public void add_sem2_results()
        {
            Add_Students_Marks f = new Add_Students_Marks(_db, _hostingEnvironment);
            f.read_excel1();
        }

        public void promote_to_next_class()
        {
            Promote_To_Next_Class f = new Promote_To_Next_Class(_db);
            f.promote_students();
        }



        [HttpPost]
        public IActionResult generate_result()
        {
            var grade = Request.Form["Grade"];
            var roll = Request.Form["Roll"];
            var c_g_n = _db.Grades.ToList();
            ViewBag.grade = c_g_n;
            if (!string.IsNullOrEmpty(grade) && !string.IsNullOrEmpty(roll))
            {
                var std = _db.PCS.Where(x => x.Grade_Name == grade && roll == x.Roll).ToList();
                return View("./Views/Add_Result.cshtml", std);
            }
            else if(string.IsNullOrEmpty(grade) && !string.IsNullOrEmpty(roll))
            {
                int roll1 = int.Parse(roll);
                var std = _db.PCS.Where(x => x.Roll == roll1).ToList();
                return View("./Views/Add_Result.cshtml", std);
            }
            else if (!string.IsNullOrEmpty(grade) && string.IsNullOrEmpty(roll))
            {
                var std = _db.PCS.Where(x => grade == x.Grade_Name).ToList();
                return View("./Views/Add_Result.cshtml", std);
            }
            else
            {
                return RedirectToAction("Print_Previous_Result", "Student");
            }


        }

        public IActionResult P_Result(Previous_Classes_of_Students s)
        {
            Models.Previous_Classes_of_Students student = _db.PCS.FirstOrDefault(x => x.Roll == s.Roll && x.Grade_Name== s.Grade_Name);
            if (student != null)
            {
                int marks = 0;
                var std = _db.Subjects.Where(x => x.Student_Roll == s.Roll && x.Class == s.Grade_Name);
                if (std.Any())
                {
                    Total total = new Total();
                    Complete_Result complete_Result = new Complete_Result();
                    //student.Date_Of_Admission = complete_Result.convert_date(student.Date_Of_Admission);
                    //student.DOB = complete_Result.convert_date(student.DOB);
                    ViewData["marks"] = std.FirstOrDefault(x => x.Semester == 1);
                    ViewBag.gender = complete_Result.gender(student.Gender);
                    ViewBag.a_promoted = complete_Result.promoted_to(student.Grade_Name);
                    ViewBag.e_promoted = complete_Result.promoted_to(student.Grade_Name, "a");
                    if (std.Count() > 1)
                    {
                        var sem1 = std.FirstOrDefault(x => x.Semester == 1);
                        ViewData["marks1"] = std.FirstOrDefault(x => x.Semester == 2);
                        var sem2 = std.FirstOrDefault(x => x.Semester == 2);
                        ViewBag.result_generated_date = complete_Result.convert_date(sem2.Result_Date);
                        marks = 100;
                        total = complete_Result.Total(sem1, sem2);
                        var r_d = sem2.Result_Date;
                        int ses = int.Parse(r_d.Substring(r_d.Length - 4));
                        ViewBag.session = ses;
                        ViewBag.session1 = ses - 1;
                        ViewBag.islamic_session = ses - 578;
                        ViewBag.islamic_session1 = ses - 579;
                        ViewBag.num = std.FirstOrDefault(x => x.Semester == 1).Class;
                    }
                    else if (std.Count() == 1)
                    {
                        var sem = std.FirstOrDefault(x => x.Semester == 1);
                        if (sem == null)
                        {
                            sem = std.FirstOrDefault(x => x.Semester == 2);
                        }
                        marks = 50;
                        total = complete_Result.Semester1(sem);
                        ViewBag.result_generated_date = complete_Result.convert_date(sem.Result_Date);
                        var arabic_grade1 = complete_Result.return_arabic_grade(s.Grade_Name);
                        ViewBag.arabic_grade = arabic_grade1;
                       
                        if (std.FirstOrDefault(x => x.Semester == 1) != null)
                        {
                            ViewBag.heading_a = "تقرير الفصل الدراسي الأول" + " " + arabic_grade1;
                            ViewBag.heading_e = "First Semester Report " + sem.Class;
                            ViewBag.sem_e = "1st Semester";
                            ViewBag.sem_a = "الفصل الدراسي الأول";
                        }
                        else
                        {
                            ViewBag.heading_a = "تقرير الفصل الدراسي الثاني" + " " + arabic_grade1;
                            ViewBag.heading_e = "Second Semester Report " + sem.Class;
                            ViewBag.sem_e = "2nd Semester";
                            ViewBag.sem_a = "الفصل الدراسي الثاني";
                        }
                        ViewData["total"] = total;
                        var r_d = sem.Result_Date;
                        int ses = int.Parse(r_d.Substring(r_d.Length - 4));
                        ViewBag.session = ses;
                        ViewBag.session1 = ses - 1;
                        ViewBag.islamic_session = ses - 578;
                        ViewBag.islamic_session1 = ses - 579;
                    }
                    ViewData["grade_letter"] = complete_Result.alphabet_grades(total, marks);
                    double cgpa = complete_Result.calculate_gpa(total, marks);
                    cgpa = Math.Round(cgpa, 1);

                    ViewData["total"] = total;
                    ViewData["cgpa"] = cgpa;
                    ViewData["credits"] = complete_Result.get_credits(total, marks);
                    ViewData["grade_letter"] = complete_Result.alphabet_grades(total, marks);
                    ViewBag.average = complete_Result.average(total.Total_Marks);
                    var arabic_grade = complete_Result.return_arabic_grade(s.Grade_Name);
                    ViewBag.arabic_grade = arabic_grade;
                    if (std.Count() == 1)
                    {
                        //return View("./Views/Result_Card_Front.cshtml", student);
                        return new Rotativa.AspNetCore.ViewAsPdf("./Views/P_Result_Card_Front.cshtml", student, viewData: ViewData)
                        {

                            PageMargins = { Left = 7, Bottom = 0, Right = 3, Top = 0 },
                            PageSize = Rotativa.AspNetCore.Options.Size.A4,
                            FileName = "Result for " + student.Name_English + "(" + student.Grade_Name + ")" + ".pdf"
                        };
                    }
                    else
                    {
                        //return View("./Views/Result_Front.cshtml", student);

                        return new Rotativa.AspNetCore.ViewAsPdf("./Views/P_Result_Front.cshtml", student, viewData: ViewData)
                        {

                            PageMargins = { Left = 7, Bottom = 0, Right = 3, Top = 0 },
                            PageSize = Rotativa.AspNetCore.Options.Size.A4,
                            FileName = "Result for " + student.Name_English + "(" + student.Grade_Name + ")" + ".pdf"
                        };
                    }

                }
                else
                {
                    return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} result not available", student.Roll) });
                }
            }
            return RedirectToAction("view_dashboard", "Student", new { message = String.Format("Student {0} result not available", student.Roll) });
        }

        public void add_country()
        {
            Get_Excel_File f = new Get_Excel_File(_db, _hostingEnvironment);
            f.countries();
        }
        public IActionResult slc(student st)
        {
            Complete_Result c = new Complete_Result();

            var d = DateTime.Now.ToString();
            d = d.Remove(d.Length - 11);
            var d1 = d.Split("/");
            d = c.convert_day_single_to_double_digit(d1[1]) + "-" + c.convert_month_to_alphabets(d1[0]) + "-" + d1[2];
            ViewData["date"] = d;
            
            List<Slc> info = new List<Slc>();
            var std_pcs = _db.PCS.Where(x => x.Roll == st.Roll).ToList();
            var std_current = _db.Students.FirstOrDefault(x => x.Roll == st.Roll);

            if (std_pcs.Any())
            {
                string current_grade = null;
                foreach (var grade1 in std_pcs)
                {
                    
                    var se = _db.Subjects.FirstOrDefault(x => x.Student_Roll == grade1.Roll && x.Semester == 2 && x.Class==grade1.Grade_Name);
                    if (se != null)
                    {
                        var g1 = se.Class;
                        var s1 = se.Result_Date.Substring(se.Result_Date.Length-4);
                        string temp_grade = s1.ToString();
                        var temp_s = int.Parse(s1) - 1;
                        s1 = temp_s + "-" + s1;
                        info.Add(new Slc {
                        grade = g1,
                        session = s1});

                        if (current_grade == null)
                        {
                            current_grade = temp_grade;
                            ViewBag.current_grade = c.promoted_to(se.Class, "a");
                        }
                        else
                        {
                            if (int.Parse(current_grade) < int.Parse(temp_grade))
                            {
                                current_grade = temp_grade;
                                ViewBag.current_grade = c.promoted_to(se.Class, "a");
                            }
                        }
                    }
                   
                }
                
                ViewBag.try1 = info;
                return new Rotativa.AspNetCore.ViewAsPdf("./Views/P_Slc.cshtml", std_pcs[0], viewData: ViewData)
                {

                    PageMargins = { Left = 12, Bottom = 5, Right = 8, Top = 5 },
                    PageSize = Rotativa.AspNetCore.Options.Size.A4,
                };
            }
            
            ViewBag.try1 = info;

            //return View("./Views/slc.cshtml", std_current);
            return new Rotativa.AspNetCore.ViewAsPdf("./Views/slc.cshtml", std_current, viewData: ViewData)
            {

                PageMargins = { Left = 12, Bottom = 5, Right = 8, Top = 5 },
                PageSize = Rotativa.AspNetCore.Options.Size.A4,
            };

        }
    }

}
