﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LMS.Models;
using LMS.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace LMS.Controllers
{
    public class LoginController : Controller
    {
       
        ClaimsIdentity identity = null;
        bool isAuthenticated = false;
        
        private DataContext _db;
        

        //DataContext db = new DataContext();
        public LoginController(DataContext db)
        {
            _db = db;
            
        }
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            return View(new Models.login());
        }

        
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Validate(login login)
        {
            Models.login l = _db.Login.FirstOrDefault(x => x.Username == login.Username);
            if(l != null)
            {
                
                if (l.Password == login.Password)
                {
                    if (l.Role == "Admin")
                    {

                            identity = new ClaimsIdentity(new[]
                            {
                            new Claim(ClaimTypes.Name, l.Username),
                            new Claim(ClaimTypes.Role, "Admin")

                        }, CookieAuthenticationDefaults.AuthenticationScheme);
                    }
                    isAuthenticated = true;

                    if (isAuthenticated)
                    {
                        var principal = new ClaimsPrincipal(identity);
                        HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                        return RedirectToAction("view_dashboard","Student");
                    }
                }
                else
                {
                    ViewBag.message = "Login InCorrect";
                    return View("./Views/Login/Login.cshtml");
                }
            }
            
            ViewBag.message = "Login InCorrect";
            return View("./Views/Login/Login.cshtml");
            
        }
        public IActionResult Logout()
        {
            isAuthenticated = false;
            HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Login");
        }
    }
}