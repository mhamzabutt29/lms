﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Models;
using Microsoft.AspNetCore.Mvc;

namespace LMS.Controllers
{
    public class CommonController : Controller
    {
        private Data.DataContext _db;
        public CommonController(Data.DataContext db)
        {
            _db = db;
        }
        public IActionResult GoBack(string Url)
        {
            return Redirect(Url);
        }

        public JsonResult CheckGradeAvailability(string g_na, string s_na)
        {
            var result = _db.Grades.Any(x => x.Grade_Name == g_na && x.Section_Name == s_na);
            if (result)
            { return Json(1); }
            else { return Json(0); }
          
            
        }
    }
}