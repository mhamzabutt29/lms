﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Models;
using Microsoft.AspNetCore.Mvc;

namespace LMS.Controllers
{
    public class GradeController : Controller
    {
        private Data.DataContext _db;
        public GradeController(Data.DataContext db)
        {
            _db = db;
        }
        [HttpPost]
        public IActionResult Index(Grade grade)
        {
            if (grade.Level != "Please Select")
            {
                var a = grade.Level + "-" + grade.Grade_Name + " (" + grade.Section_Name + ")";
                if (_db.Grades.Any(x => x.Complete_Grade_Name == a))
                {
                    ViewBag.message = "Grade already exist";
                }
                else
                {
                    grade.Complete_Grade_Name = a;
                    grade.Complete_Grade_Name_Arabic = arabic_grade(a);
                    _db.Grades.Add(grade);
                    _db.SaveChanges();
                    ViewBag.message = "Grade saved";
                }
            }
            else
            {
                ViewBag.message = "Select Level";
            }
            var section = _db.Sections.ToList();
                ViewBag.section = section;
                var g = _db.Grades.ToList().OrderBy(x => x.Grade_Name);
                ViewBag.grade = g;
            
            

            return View("./Views/Grades/Grades.cshtml", new Models.Grade());

        }

        public string arabic_grade(string a)
        {
            //////////////////////////////KG-1/////////////////////////////////////
            if (a == "KG-1 (A)")
            {
                return "(روضة أول (ا";
            }
            else if (a == "KG-1 (B)")
            {
                return "(روضة أول (ب";
            }
            else if (a == "KG-1 (C)")
            {
                return "(روضة أول (ج";
            }
            else if (a == "KG-1 (D)")
            {
                return "(روضة أول (د";
            }

            /////////////////////////////KG-2/////////////////////
            else if (a == "KG-2 (A)")
            {
                return "(روضة ثاني (ا";
            }
            else if (a == "KG-2 (B)")
            {
                return "(روضة ثاني (ب";
            }
            else if (a == "KG-2 (C)")
            {
                return "(روضة ثاني (ج";
            }
            else if (a == "KG-2 (D)")
            {
                return "(روضة ثاني (د";
            }
            /////////////////////////////////KG-3//////////////////////////////
            else if (a == "KG-3 (A)")
            {
                return "(روضة ثالث (ا";
            }
            else if (a == "KG-3 (B)")
            {
                return "(روضة ثالث (ب";
            }
            else if (a == "KG-3 (C)")
            {
                return "(روضة ثالث (ج";
            }
            else if (a == "KG-3 (D)")
            {
                return "(روضة ثالث (د";
            }
            /////////////////////////////////////Grade 1/////////////////////////////
            else if (a == "Grade-1 (A)")
            {
                return " (الصف الاول الابتدائي (ا";
            }
            else if (a == "Grade-1 (B)")
            {
                return "(الصف الاول الابتدائي (ب";
            }
            else if (a == "Grade-1 (C)")
            {
                return "(الصف الاول الابتدائي (ج";
            }
            else if (a == "Grade-1 (D)")
            {
                return "(الصف الاول الابتدائي (د";
            }
            /////////////////////////////////////Grade-2/////////////////////////////
            else if (a == "Grade-2 (A)")
            {
                return " (الصف الثاني الابتدائي (ا";
            }
            else if (a == "Grade-2 (B)")
            {
                return "(الصف الثاني الابتدائي (ب";
            }
            else if (a == "Grade-2 (C)")
            {
                return "(الصف الثاني الابتدائي (ج";
            }
            else if (a == "Grade-2 (D)")
            {
                return "(الصف الثاني الابتدائي (د";
            }
            ////////////////////////////////////////////Grade-3//////////////////////
            else if (a == "Grade-3 (A)")
            {
                return " (الصف الثالث الابتدائي (ا";
            }
            else if (a == "Grade-3 (B)")
            {
                return "(الصف الثالث الابتدائي (ب";
            }
            else if (a == "Grade-3 (C)")
            {
                return "(الصف الثالث الابتدائي (ج";
            }
            else if (a == "Grade-3 (D)")
            {
                return "(الصف الثالث الابتدائي (د";
            }
            ////////////////////////////////////////////Grade-4//////////////////////
            else if (a == "Grade-4 (A)")
            {
                return " (الصف الرابه الابتدائي (ا";
            }
            else if (a == "Grade-4 (B)")
            {
                return "(الصف الرابه الابتدائي (ب";
            }
            else if (a == "Grade-4 (C)")
            {
                return "(الصف الرابه الابتدائي (ج";
            }
            else
            {
                return "(الصف الرابه الابتدائي (د";
            }
        }
    }
}