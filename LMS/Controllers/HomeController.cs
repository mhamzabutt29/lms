﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LMS.Models;
using Rotativa.AspNetCore;
using Microsoft.AspNetCore.Authorization;

namespace LMS.Controllers
{
   [Authorize(Roles ="Admin")]
    public class HomeController : Controller
    {
        private Data.DataContext _db;
        public HomeController(Data.DataContext db)
        {
            _db = db;
        }
        public ActionResult Home()
        {
            return RedirectToAction("view_dashboard","Student");
        }
        public IActionResult Grades()
        {
            var section = _db.Sections.ToList();
            ViewBag.section = section;
            var grade = _db.Grades.ToList().OrderBy(x=>x.Grade_Name);
            ViewBag.grade = grade;
            return View("./Views/Grades/Grades.cshtml", new Models.Grade());

        }
        
        public IActionResult converttopdf(student s)
        {
            ViewBag.marks = _db.Subjects.FirstOrDefault(x => x.Student_Roll == 1);
            return new ViewAsPdf("./Views/Result_Front.cshtml", new Models.student())
            {

                PageMargins = { Left = 5, Bottom = 5, Right = 5, Top = 5 },
                PageSize = Rotativa.AspNetCore.Options.Size.A4
            };

        }

        public IActionResult Add_Students_In_Bulk()
        {
            return View("./Views/Add_Students.cshtml");
        }

    }
}

