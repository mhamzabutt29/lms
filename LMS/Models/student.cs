﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class student
    {
        public student()
        {
            IsActive = "Yes"; 
        }
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Required]
        public int Roll { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The Name must be less than 50 characters.")]
        public string Name_English { get; set; }

        [Required]
        public string Name_Arabic { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public string Nationality_English { get; set; }

        [Required]
        public string Nationality_Arabic { get; set; }

        [Required]
        public string DOB { get; set; }

        [Required]
        public string POB { get; set; }

        [Required]
        public string Passport_Number { get; set; }

        [Required]
        public string Iqama_Number { get; set; }

        [Required]
        public string Date_Of_Admission { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "The Previous School must be less than 50 characters.")]
        public string Previous_School_English { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "The Previous School must be less than 50 characters.")]
        public string Previous_School_Arabic { get; set; }

        [Required]
        public string Grade_Name { get; set; }
        [Required]
        public string Grade_Name_Arabic { get; set; }
        [Required]
        public string phone { get; set; }
        
        [Required]
        
        public string IsActive { get; set; }

    }
}
