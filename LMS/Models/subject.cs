﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class Subject
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Key]
        public int Subject_Id { get; set; }
        [Required]
        public int Quran { get; set; }
        [Required]
        public int Islamic_Studies { get; set; }
        [Required]
        public int Arabic { get; set; }
        [Required]
        public int English { get; set; }
        [Required]
        public int Maths { get; set; }
        [Required]
        public int Science { get; set; }
        [Required]
        public int Social { get; set; }
        [Required]
        public int Computer { get; set; }
        [Required]
        public int Arts { get; set; }
        [Required]
        public int Conduct { get; set; }
        [Required]
        public int Attendance { get; set; }
        [Required]
        public int Student_Roll { get; set; }
        [Required]
        public int Semester { get; set; }
        [Required]
        public string Class { get; set; } // Grade name with section
        public string Grade { get; set; } // Grade A, B, C, D or W depends on GPA
        [Required]
        public string IsActive { get; set; }
        [Required]
        public string Result_Date { get; set; }
    }
}
