﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class Grade
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Grade_Id { get; set; }
        [Required]
        public string Grade_Name { get; set; }
        [Required]
        public string Level { get; set; }
        [Required]
        public string Section_Name { get; set; }
        [Required]
        public string Complete_Grade_Name { get; set; }
        [Required]
        public string Complete_Grade_Name_Arabic { get; set; }
        
    }
}
