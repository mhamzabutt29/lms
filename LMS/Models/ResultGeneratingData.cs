﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class ResultGeneratingData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [Required]
        public string Generating_date { get; set; }
        [Required]
        public string Student_Id { get; set; }
        [Required]
        public string Grade { get; set; }
        [Required]
        public string Session_Year { get; set; }

    }
}
