﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LMS.Models;                             

namespace LMS.Data
{
    public class DataContext: DbContext
    {
      

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<student> Students { get; set; }
        public DbSet<login> Login { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Semester> Semesters { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<ResultGeneratingData> RGD { get; set; }
        public DbSet<Previous_Classes_of_Students> PCS { get; set; }
        public DbSet<Countries> Countries { get; set; }
    }
}
