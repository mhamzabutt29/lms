﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Helping_classes
{
    public class Total
    {

        public int Quran { get; set; }
        public int Islamic_Studies { get; set; }
        public int Arabic { get; set; }
        public int English { get; set; }
        public int Maths { get; set; }
        public int Science { get; set; }
        public int Social { get; set; }
        public int Computer { get; set; }
        public int Arts { get; set; }
        public int Conduct { get; set; }
        public int Attendance { get; set; }
        public int Total_Sem_1 { get; set; }
        public int Total_Sem_2 { get; set; }
        public int Total_Marks { get; set; } // Grade name with section
        public string Grade { get; set; } // Grade A, B, C, D or W depends on GPA
    }
}
