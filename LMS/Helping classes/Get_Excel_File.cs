﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LMS.Helping_classes
{
    public class Get_Excel_File
    {
        private Data.DataContext _db;
        private readonly IHostingEnvironment _hostingEnvironment;

        public Get_Excel_File(Data.DataContext db, IHostingEnvironment hostingEnvironment)
        {
            _db = db;
            _hostingEnvironment = hostingEnvironment;
        }
        public void read_excel()
        {
            string filep = _hostingEnvironment.WebRootPath;
            string filepath = filep + "/files/Students.xlsx";
            int flag = 0;
            
            IWorkbook workbook;
            FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            workbook = new XSSFWorkbook(fs);
            ISheet sheet = workbook.GetSheetAt(0);
            if (sheet != null)
            {
                
                int rowCount = sheet.LastRowNum; // This may not be valid row count.
                                                 // If first row is table head, i starts from 1
                for (int i = 1; i <= rowCount; i++)
                //for (int i =2; i< 10; i++)
                {
                    IRow curRow = sheet.GetRow(i);
                    // Works for consecutive data. Use continue otherwise 
                    if (curRow == null)
                    {
                        // Valid row count
                        rowCount = i - 1;
                        break;
                    }
                    string  e_n = null, a_n = null, n_e = null, pob=null, n_a = null, dob = null, p = null, doa = null, ps_e = null, ps_a= null, iq = null, g_n_a = null, g_n_e=null, gender=null, mobile=null;
                    int id = 0;
                    // Get data from the 4th column (4th cell of each row)
                    DataFormatter formatter = new DataFormatter();
                    for (int j = 0; j <= 15; j++)
                    {

                        if (String.IsNullOrEmpty(curRow.GetCell(0).NumericCellValue.ToString()) || curRow.GetCell(0).NumericCellValue <= 0)
                        {
                            flag = 1;
                            break;
                        }

                        if ( j != 0 && j != 7 && j != 12)
                        {
                            //var cell_type = curRow.GetType();
                            string cellValue = null;
                            if (String.IsNullOrEmpty(curRow.GetCell(j).CellType.ToString()))
                            {
                                break;
                            }
                            else if (curRow.GetCell(j).CellType.ToString() == "Numeric")
                            {
                                cellValue = curRow.GetCell(j).NumericCellValue.ToString();
                            }
                            else
                                cellValue = curRow.GetCell(j).StringCellValue;
                            if (j == 1)
                                g_n_e = cellValue;
                            else if (j == 2)
                                g_n_a = cellValue;
                            else if (j == 3)
                                e_n = cellValue;
                            else if (j == 4)
                                a_n = cellValue;
                            else if (j == 5)
                                n_e = cellValue;
                            else if (j == 6)
                                n_a = cellValue;
                            else if (j == 8)
                                pob = cellValue;
                            else if (j == 9)
                                gender = cellValue;
                            else if (j == 10)
                                p = cellValue;
                            else if (j == 11)
                            {
                                iq = cellValue;
                                iq = Regex.Replace(iq, @"[^0-9a-zA-Z]+", "");
                            }
                            else if (j == 13)
                                ps_e = cellValue;
                            else if (j == 14)
                                ps_a = cellValue;
                            else if (j == 15)
                                mobile = cellValue;
                        }
                        else if (j == 0)
                        {
                            var val = curRow.GetCell(j).NumericCellValue.ToString();
                            id = int.Parse(val);
                        }
                        else if (j == 7)
                        {
                            string val = null;
                            
                            val = curRow.GetCell(j).CellType.ToString();
                            if (val == "Numeric")
                            {
                                var temp = curRow.GetCell(j).DateCellValue;
                                string d = temp.ToString();
                                if (d.Contains(","))
                                {
                                    string[] a = d.ToString().Split(',');
                                    dob = a[0].ToString().Trim();
                                    dob = dob.Replace("/", "-");
                                    pob = a[1].ToString().Trim();
                                }
                                else
                                {
                                    Complete_Result c = new Complete_Result();
                                    dob = d.Remove(d.Length - 12);
                                    var d1 = dob.Split("/");
                                    dob = d1[1] + "-" + c.convert_month_to_alphabets(d1[0]) + "-" + d1[2];
                                }
                            }
                            else if (val == "String")
                            {
                                var temp = curRow.GetCell(j).StringCellValue;
                                string d = temp.ToString();
                                if (d.Contains(","))
                                {
                                    string[] a = d.ToString().Split(',');
                                    dob = a[0].ToString().Trim();
                                    pob = a[1].ToString().Trim();
                                }
                                else
                                {
                                    Complete_Result c = new Complete_Result();
                                    dob = d.Remove(d.Length - 12);
                                    var d1 = dob.Split("/");
                                    dob = d1[1] + "-" + c.convert_month_to_alphabets(d1[0]) + "-" + d1[2];
                                }
                                
                            }

                        }
                        else if (j == 12)
                        {
                            //var val = curRow.GetCell(j).StringCellValue();

                            string val = null;
                            val = curRow.GetCell(j).CellType.ToString();
                            if (val == "Numeric")
                            {
                                var temp = curRow.GetCell(j).DateCellValue;
                                string val1 = temp.ToString();
                                if (val1.Length > 11)
                                {
                                    val1 = val1.Remove(val1.Length - 12);

                                }
                                doa = val1;
                                Complete_Result c = new Complete_Result();
                                var d1 = doa.Split("/");
                                doa = d1[1] + "-" + c.convert_month_to_alphabets(d1[0]) + "-" + d1[2];

                            }
                            else if (val == "String")
                            {
                                var temp = curRow.GetCell(j).StringCellValue;
                                string val1 = temp.ToString();
                                if (val1.Length > 11)
                                {
                                    val1 = val1.Remove(val1.Length - 12);

                                }
                                doa = val1;
                                Complete_Result c = new Complete_Result();
                                var d1 = doa.Split("/");
                                doa = d1[1] + "-" + c.convert_month_to_alphabets(d1[0]) + "-" + d1[2];
                            }
                            else
                            {
                                var temp = curRow.GetCell(j).NumericCellValue;
                                string val1 = temp.ToString();
                                if (val1.Length > 11)
                                {
                                    val1 = val1.Remove(val1.Length - 12);

                                }
                                doa = val1;
                                Complete_Result c = new Complete_Result();
                                var d1 = doa.Split("/");
                                doa = d1[1] + "-" + c.convert_month_to_alphabets(d1[0]) + "-" + d1[2];
                            }
                            //var d = doa.Split("-");
                            //doa = d[2] + "-" + d[1] + "-" + d[0];
                        }
                        
                    }
                    if(flag==1)
                    { break; }
                    Models.student std =
                        new Models.student
                        {
                            Roll = id,
                            Name_English = e_n,
                            Name_Arabic = a_n,
                            Nationality_English = n_e,
                            Nationality_Arabic = n_a,
                            IsActive = "Yes",
                            DOB = dob,
                            POB = pob,
                            Passport_Number = p,
                            Iqama_Number = iq,
                            Date_Of_Admission = doa,
                            Previous_School_English = ps_e,
                            Previous_School_Arabic = ps_a,
                            Grade_Name = g_n_e,
                            Grade_Name_Arabic = g_n_a,
                            Gender = gender,
                            phone = mobile
                        };

                    var s = _db.Students.FirstOrDefault(x => x.Roll == std.Roll);
                    if(s==null)
                    {
                        var c = std.Grade_Name;
                        if (c.Contains("-"))
                        { }
                        else
                        {
                            var c_temp = c.Split(" ");
                            std.Grade_Name = c_temp[0] + "-" + c_temp[1] + " " + c_temp[2];
                        }
                        
                        _db.Students.Add(std);
                        _db.SaveChanges();
                       
                    }
                    
                }
            }
        }
        public void countries()
        {
            string filep = _hostingEnvironment.WebRootPath;
            string filepath = filep + "/files/countries.xlsx";
            IWorkbook workbook;
            FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            workbook = new XSSFWorkbook(fs);
            ISheet sheet = workbook.GetSheetAt(0);
            if (sheet != null)
            {

                int rowCount = sheet.LastRowNum; // This may not be valid row count.
                                                 // If first row is table head, i starts from 1
                for (int i = 0; i <= rowCount; i++)
                //for (int i =2; i< 10; i++)
                {
                    IRow curRow = sheet.GetRow(i);
                    // Works for consecutive data. Use continue otherwise 
                    if (curRow == null)
                    {
                        // Valid row count
                        rowCount = i - 1;
                        break;
                    }
                    string c=null;
                    // Get data from the 4th column (4th cell of each row)
                    DataFormatter formatter = new DataFormatter();
                    for (int j = 0; j < 1; j++)
                    {
                            string cellValue = null;
                            if (curRow.GetCell(j).CellType.ToString() == "Numeric")
                            {
                                cellValue = curRow.GetCell(j).NumericCellValue.ToString();
                            }
                            else
                                cellValue = curRow.GetCell(j).StringCellValue;
                            Console.WriteLine(cellValue);
                            if (j == 0)
                                c = cellValue;
                            

                    }
                    if (String.IsNullOrEmpty(c))
                    { break; }
                    Models.Countries ctry =
                        new Models.Countries
                        {
                            Country = c
                        };

                    _db.Countries.Add(ctry);
                    _db.SaveChanges();
                    
                }
            }
        }
    }

    
}
