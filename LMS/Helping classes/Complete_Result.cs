﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Helping_classes
{
    public class Complete_Result
    {
        public Total Total(Models.Subject sem1, Models.Subject sem2)
        {
            Total total = new Total();
            total.Quran = sem1.Quran + sem2.Quran;
            total.Maths = sem1.Maths + sem2.Maths;
            total.Science = sem1.Science + sem2.Science;
            total.Islamic_Studies = sem1.Islamic_Studies + sem2.Islamic_Studies;
            total.English = sem1.English + sem2.English;
            total.Social = sem1.Social + sem2.Social;
            total.Arabic = sem1.Arabic + sem2.Arabic;
            total.Arts = sem1.Arts + sem2.Arts;
            total.Attendance = sem1.Attendance + sem2.Attendance;
            total.Computer = sem1.Computer + sem2.Computer;
            total.Conduct = sem1.Conduct + sem2.Conduct;
            
            total.Total_Sem_1 = sem1.Quran + sem1.Maths + sem1.Science + sem1.Islamic_Studies + sem1.English + sem1.Social + sem1.Arabic + sem1.Arts + sem1.Computer;
            total.Total_Sem_2 = sem2.Quran + sem2.Maths + sem2.Science + sem2.Islamic_Studies + sem2.English + sem2.Social + sem2.Arabic + sem2.Arts + sem2.Computer;
            total.Total_Marks = total.Total_Sem_1 + total.Total_Sem_2;
            return total;
        }
        public Total Semester1(Models.Subject sem1)
        {
            Total total = new Total();
            total.Quran = sem1.Quran + 0;
            total.Maths = sem1.Maths + 0;
            total.Science = sem1.Science + 0;
            total.Islamic_Studies = sem1.Islamic_Studies + 0;
            total.English = sem1.English + 0;
            total.Social = sem1.Social + 0;
            total.Arabic = sem1.Arabic + 0;
            total.Arts = sem1.Arts + 0;
            total.Attendance = sem1.Attendance + 0;
            total.Computer = sem1.Computer + 0;
            total.Conduct = sem1.Conduct + 0;
            total.Total_Sem_1 = sem1.Quran + sem1.Maths + sem1.Science + sem1.Islamic_Studies + sem1.English + sem1.Social + sem1.Arabic + sem1.Arts + sem1.Computer;
            total.Total_Marks = total.Total_Sem_1 + 0;
            return total;
        }
        
        public Grading get_credits(Total total, int marks)
        {
            Grading credits = new Grading();
            credits.Quran = calculate_grade(total.Quran, marks);
            credits.Islamic_Studies = calculate_grade(total.Islamic_Studies, marks);
            credits.English = calculate_grade(total.English, marks);
            credits.Maths = calculate_grade(total.Maths, marks);
            credits.Science = calculate_grade(total.Science, marks);
            credits.Social = calculate_grade(total.Social, marks);
            credits.Arabic = calculate_grade(total.Arabic, marks);
            credits.Computer = calculate_grade(total.Computer, marks);
            credits.Arts = calculate_grade(total.Arts, marks);
            return credits;
        }

        public double calculate_grade(int marks, int total_marks)
        {
            double result = 0.0;
            double percent = 0;
            percent = (int)Math.Round((double)(100 * marks) / total_marks);
            if (percent >= 90 && percent <= 100)
            {
                result = 4;
            }
            else if (percent >= 80 && percent < 90)
            {
                result = 3.5;
            }
            else if (percent >= 70 && percent < 80)
            {
                result = 3;
            }
            else if (percent >= 60 && percent < 70)
            {
                result = 2;
            }
            else if (percent >= 50 && percent < 60)
            {
                result = 1;
            }
            else
            {
                result = 0.0;
            }
            return result;
        }
        public double calculate_gpa(Total marks, int total_marks)
        {
            //Quran+Isl+Arabic+English+Maths+Science+SS+Computer+Arts
            int total_gpa = 4 + 4 + 3 + 4 + 4 + 4 + 4 + 3 + 3;


            double Quran = calculate_grade(marks.Quran, total_marks) * 4;
            double Arabic = calculate_grade(marks.Arabic, total_marks) * 3;
            double Islamic_Studies = calculate_grade(marks.Islamic_Studies, total_marks) * 4;
            double English = calculate_grade(marks.English, total_marks) * 4;
            double Maths = calculate_grade(marks.Maths, total_marks) * 4;
            double Science = calculate_grade(marks.Science, total_marks) * 4;
            double Social_Studies = calculate_grade(marks.Social, total_marks) * 4;
            double Computer = calculate_grade(marks.Computer, total_marks) * 3;
            double Arts = calculate_grade(marks.Arts, total_marks) * 3;


            double total = (Quran + Arabic + Islamic_Studies + English + Maths + Science + Social_Studies + Computer + Arts) / total_gpa;
            return total;

        }

        public grade_letter alphabet_grades(Total total, int marks)
        {
            grade_letter g = new grade_letter();
            g.Quran = grade_letter(total.Quran, marks);
            g.Islamic_Studies = grade_letter(total.Islamic_Studies, marks);
            g.English = grade_letter(total.English, marks);
            g.Maths = grade_letter(total.Maths, marks);
            g.Science = grade_letter(total.Science, marks);
            g.Social = grade_letter(total.Social, marks);
            g.Arabic = grade_letter(total.Arabic, marks);
            g.Computer = grade_letter(total.Computer, marks);
            g.Arts = grade_letter(total.Arts, marks);
            g.Conduct = grade_letter(total.Conduct, marks);
            g.Attendance = grade_letter(total.Attendance, marks);

            return g;
        }
        public string grade_letter(int marks, int total_marks)
        {
            string result = null;
            double percent = 0;
            percent = (int)Math.Round((double)(100 * marks) / total_marks);
            if (percent >= 90 && percent <= 100)
            {
                result = "A+";
            }
            else if (percent >= 80 && percent < 90)
            {
                result = "A";
            }
            else if (percent >= 70 && percent < 80)
            {
                result = "B";
            }
            else if (percent >= 60 && percent < 70)
            {
                result = "C";
            }
            else if (percent >= 50 && percent < 60)
            {
                result = "D";
            }
            else
            {
                result = "F";
            }
            return result;
        }

        public string gender(string gender)
        {
            if (gender == "Male")
               return "ناجح يرفع";
            else
               return "ناجحة ترفع";
        }
        public string promoted_to(string grade, string a=null)
        {
            if (a == null)
            {
                if (grade.Contains("KG-1") || grade.Contains("KG 1"))
                {
                    return "روضة ثاني";
                }
                else if (grade.Contains("KG-2") || grade.Contains("KG 2"))
                {
                    return "روضة ثالث";
                }
                else if (grade.Contains("KG-3") || grade.Contains("KG 3"))
                {
                    return "الصف الاول  ";
                }
                else if (grade.Contains("Grade-1") || grade.Contains("Grade 1"))
                {
                    return "الصف الثاني  ";
                }
                else if (grade.Contains("Grade-2") || grade.Contains("Grade 2"))
                {
                    return "الصف الثالث  ";
                }
                else if (grade.Contains("Grade-3") || grade.Contains("Grade 3"))
                {
                    return "الصف الرابه ";
                }
                else
                    return "الصف الخامس";
            }
            else
            {
                if (grade.Contains("KG-1") || grade.Contains("KG 1"))
                {
                    return "KG-2";
                }
                else if (grade.Contains("KG-2") || grade.Contains("KG 2"))
                {
                    return "KG-3";
                }
                else if (grade.Contains("KG-3") || grade.Contains("KG 3"))
                {
                    return "Grade-1";
                }
                else if (grade.Contains("Grade-1")||  grade.Contains("Grade 1"))
                {
                    return "Grade-2";
                }
                else if (grade.Contains("Grade-2") || grade.Contains("Grade 2"))
                {
                    return "Grade-3";
                }
                else if (grade.Contains("Grade-3") || grade.Contains("Grade 3"))
                {
                    return "Grade-4";
                }
                else
                    return "Grade";
            }
        }

        public string convert_month_to_alphabets(string a)
        {
            Dictionary<string, string> month = new Dictionary<string, string>();
            month.Add("01", "Jan");
            month.Add("02", "Feb");
            month.Add("03", "Mar");
            month.Add("04", "Apr");
            month.Add("05", "May");
            month.Add("06", "Jun");
            month.Add("07", "Jul");
            month.Add("08", "Aug");
            month.Add("09", "Sep");
            month.Add("10", "Oct");
            month.Add("11", "Nov");
            month.Add("12", "Dec");
            month.Add("1", "Jan");
            month.Add("2", "Feb");
            month.Add("3", "Mar");
            month.Add("4", "Apr");
            month.Add("5", "May");
            month.Add("6", "Jun");
            month.Add("7", "Jul");
            month.Add("8", "Aug");
            month.Add("9", "Sep");
            month.Add("Jan", "Jan");
            month.Add("Feb", "Feb");
            month.Add("Mar", "Mar");
            month.Add("Apr", "Apr");
            month.Add("May", "May");
            month.Add("Jun", "Jun");
            month.Add("Jul", "Jul");
            month.Add("Aug", "Aug");
            month.Add("Sep", "Sep");
            month.Add("Oct", "Oct");
            month.Add("Nov", "Nov");
            month.Add("Dec", "Dec");

            try
            {
                return month[a];
            }
            catch (KeyNotFoundException)
            {
                return "false";
            }
            
        }
        
        public string convert_date(string d)
        {
            var temp = d.Split("-");
            temp[1] = convert_month_to_alphabets(temp[1]);
            string s = temp[0] + "-" + temp[1] + "-" + temp[2];
            return s;
        }

        public string return_arabic_grade(string grade)
        {
            if (grade.Contains("KG-1")  || grade.Contains("KG 1"))
            {
                return "روضة الاول";
            }
            else if (grade.Contains("KG-2") || grade.Contains("KG 2"))
            {
                return "روضة ثاني";
            }
            else if (grade.Contains("KG-3") || grade.Contains("KG 3"))
            {
                return "روضة ثالث";
            }
            else if (grade.Contains("Grade-1") || grade.Contains("Grade 1"))
            {
                return "الصف الاول  ";
            }
            else if (grade.Contains("Grade-2") || grade.Contains("Grade 2"))
            {
                return "الصف الثاني  ";
            }
            else if (grade.Contains("Grade-3") || grade.Contains("Grade 3"))
            {
                return "الصف ثالث ";
            }
            else if (grade.Contains("Grade-4") || grade.Contains("Grade 4"))
            {
                return "الصف الرابه ";
            }
            else 
            {
                return "الصف الخامس  ";
            }


        }

        public string average(int marks)
        {
            double m = Convert.ToDouble(marks);
            double t = 0.00;
            t = m / 900;
            t = t * 100;
            t = Math.Round(t, 1);
            var s = t.ToString();
            s = s + "%";
            return s;
        }

        public string convert_month_alphabets_to_number(string a)
        {
            Dictionary<string, string> month = new Dictionary<string, string>();
            month.Add("Jan", "01");
            month.Add("Feb", "02");
            month.Add("Mar", "03");
            month.Add("Apr", "04");
            month.Add("May", "05");
            month.Add("Jun", "06");
            month.Add("Jul", "07");
            month.Add("Aug", "08");
            month.Add("Sep", "09");
            month.Add("Oct", "10");
            month.Add("Nov", "11");
            month.Add("Dec", "12");

            try
            {
                return month[a].ToString();
            }
            catch (KeyNotFoundException)
            {
                return "false";
            }

        }

        public string convert_day_single_to_double_digit(string a)
        {
            Dictionary<string, string> month = new Dictionary<string, string>();
            month.Add("1", "01");
            month.Add("2", "02");
            month.Add("3", "03");
            month.Add("4", "04");
            month.Add("5", "05");
            month.Add("6", "06");
            month.Add("7", "07");
            month.Add("8", "08");
            month.Add("9", "09");
            month.Add("01", "01");
            month.Add("02", "02");
            month.Add("03", "03");
            month.Add("04", "04");
            month.Add("05", "05");
            month.Add("06", "06");
            month.Add("07", "07");
            month.Add("08", "08");
            month.Add("09", "09");

            try
            {
                return month[a].ToString();
            }
            catch (KeyNotFoundException)
            {
                return a;
            }

        }

    }
}
