﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Helping_classes;

namespace LMS.Helping_classes
{
    public class Promote_To_Next_Class
    {
        private Data.DataContext _db;
        public Promote_To_Next_Class(Data.DataContext db)
        {
            _db = db;
        }

        public void promote_students()
        {
            //Models.student students = new Models.student();
            var students = _db.Students.ToList();
            //Models.Previous_Classes_of_Students p = new Models.Previous_Classes_of_Students();
            foreach (var student in students)
            {
                if (student.IsActive == "Yes")
                {
                    Models.Previous_Classes_of_Students p = new Models.Previous_Classes_of_Students {
                        Roll = student.Roll,
                        Name_English = student.Name_English,
                        Name_Arabic = student.Name_Arabic,
                        Date_Of_Admission = student.Date_Of_Admission,
                        DOB = student.DOB,
                        POB = student.POB,
                        Previous_School_Arabic = student.Previous_School_Arabic,
                        Previous_School_English = student.Previous_School_English,
                        Nationality_English = student.Nationality_English,
                        Nationality_Arabic = student.Nationality_Arabic,
                        Passport_Number = student.Passport_Number,
                        Iqama_Number = student.Iqama_Number,
                        phone = student.phone,
                        IsActive = student.IsActive,
                        Grade_Name = student.Grade_Name,
                        Grade_Name_Arabic = student.Grade_Name_Arabic,
                        Gender = student.Gender,
                    };
                    var section = p.Grade_Name.Substring(p.Grade_Name.Length - 3);
                    var section_arabic = p.Grade_Name_Arabic.Substring(p.Grade_Name_Arabic.Length - 3);
                    var std_in_pcs = _db.PCS.Where(x => x.Roll == p.Roll && x.Grade_Name == p.Grade_Name).ToList();
                    if (!std_in_pcs.Any())
                    {
                        var p_result = _db.Subjects.FirstOrDefault(x => x.Student_Roll == p.Roll && x.Class == p.Grade_Name && x.Semester == 2);
                        if (p_result != null)
                        {
                            _db.PCS.Add(p);
                            var s = student;
                            if (student.Grade_Name.Contains("Grade-3"))
                            {
                                _db.Students.Remove(student);
                            }
                            else
                            {
                                Complete_Result c = new Complete_Result();
                                student.Grade_Name = c.promoted_to(student.Grade_Name, "a");
                                student.Grade_Name = student.Grade_Name + " " + section;
                                var arabic_grade = _db.Grades.FirstOrDefault(x => x.Complete_Grade_Name == student.Grade_Name);
                                student.Grade_Name_Arabic = arabic_grade.Complete_Grade_Name_Arabic;
                                //student.Grade_Name_Arabic = c.promoted_to(student.Grade_Name);
                                //student.Grade_Name_Arabic = student.Grade_Name_Arabic + " " + section_arabic;

                                _db.Students.Remove(student);
                                _db.Students.Update(s);
                            }

                            _db.SaveChanges();
                        }

                    }
                    //var s = student;
                    //if (student.Grade_Name.Contains("Grade-3"))
                    //{
                    //    _db.Students.Remove(student);
                    //}
                    //else
                    //{
                    //    Complete_Result c = new Complete_Result();
                    //    student.Grade_Name = c.promoted_to(student.Grade_Name, "a");
                    //    student.Grade_Name = student.Grade_Name + " " + section;
                    //    var arabic_grade = _db.Grades.FirstOrDefault(x => x.Complete_Grade_Name == student.Grade_Name);
                    //    student.Grade_Name_Arabic = arabic_grade.Complete_Grade_Name_Arabic;
                    //    //student.Grade_Name_Arabic = c.promoted_to(student.Grade_Name);
                    //    //student.Grade_Name_Arabic = student.Grade_Name_Arabic + " " + section_arabic;

                    //    _db.Students.Remove(student);
                    //    _db.Students.Update(s);
                    //}

                    //_db.SaveChanges();


                }
            }
        }

    }
}
