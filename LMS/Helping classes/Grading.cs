﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Helping_classes
{
    public class Grading
    {
        public double Quran { get; set; }
        public double Islamic_Studies { get; set; }
        public double Arabic { get; set; }
        public double English { get; set; }
        public double Maths { get; set; }
        public double Science { get; set; }
        public double Social { get; set; }
        public double Computer { get; set; }
        public double Arts { get; set; }
        
    }
}
