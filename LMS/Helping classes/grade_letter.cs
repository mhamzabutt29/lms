﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Helping_classes
{
    public class grade_letter
    {
        public string Quran { get; set; }
        public string Islamic_Studies { get; set; }
        public string Arabic { get; set; }
        public string English { get; set; }
        public string Maths { get; set; }
        public string Science { get; set; }
        public string Social { get; set; }
        public string Computer { get; set; }
        public string Arts { get; set; }
        public string Conduct { get; set; }
        public string Attendance { get; set; }

    }
}
