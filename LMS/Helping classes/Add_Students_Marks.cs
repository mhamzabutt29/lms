﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;

namespace LMS.Helping_classes
{
    public class Add_Students_Marks
    {
        private Data.DataContext _db;
        private readonly IHostingEnvironment _hostingEnvironment;

        public Add_Students_Marks(Data.DataContext db, IHostingEnvironment hostingEnvironment)
        {
            _db = db;
            _hostingEnvironment = hostingEnvironment;
        }
        public void read_excel()
        {
            string filep = _hostingEnvironment.WebRootPath;
            string filepath = filep + "/files/Semester1.xlsx";
            IWorkbook workbook;
            FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            workbook = new XSSFWorkbook(fs);
            ISheet sheet = workbook.GetSheetAt(0);
            if (sheet != null)
            {

                int rowCount = sheet.LastRowNum; // This may not be valid row count.
                                                 // If first row is table head, i starts from 1
                for (int i = 1; i <= rowCount; i++)
                {
                    IRow curRow = sheet.GetRow(i);
                    // Works for consecutive data. Use continue otherwise 
                    if (curRow == null)
                    {
                        // Valid row count
                        rowCount = i - 1;
                        break;
                    }
                    int quran = 0, english = 0, Sci = 0, Isl = 0, arabic = 0, maths = 0, conduct = 0, attendance = 0, computer = 0, SS = 0, arts = 0; 
                    int id = 0; 
                    string className = null, result_date=null;
                    // Get data from the 4th column (4th cell of each row)
                    DataFormatter formatter = new DataFormatter();
                    for (int j = 0; j <= 14; j++)
                    {

                     
                        var cell_type = curRow.GetType();
                        string cellValue = null;
                        if (j == 14)
                        {
                            cellValue = curRow.GetCell(j).DateCellValue.ToString();
                            cellValue = cellValue.Remove(cellValue.Length - 12);
                        }
                        else
                        {
                            if (curRow.GetCell(j).CellType.ToString() == "Numeric")
                            {
                                cellValue = curRow.GetCell(j).NumericCellValue.ToString();
                            }
                            else
                                cellValue = curRow.GetCell(j).StringCellValue;
                        }
                        int val = 0;
                        if (j != 0 && j != 1 && j != 2 && j != 14)
                        {
                            if (cellValue.Contains("."))
                            {
                                var input = cellValue.Substring(0, cellValue.IndexOf(".") + 1);
                                input = input.Replace(".", "");
                                val = int.Parse(input);
                            }
                            else
                                val = int.Parse(cellValue);
                        }

                        if (j == 0)
                            id = int.Parse(cellValue);
                        else if (j == 1)
                            className = cellValue;
                        else if (j == 3)
                            quran = val;// int.Parse(cellValue);
                        else if (j == 4)
                            Isl = val;// int.Parse(cellValue);
                        else if (j == 5)
                            arabic = val;// int.Parse(cellValue);
                        else if (j == 6)
                            english = val;//int.Parse(cellValue);
                        else if (j == 7)
                            maths = val;//int.Parse(cellValue);
                        else if (j == 8)
                            Sci = val;//int.Parse(cellValue);
                        else if (j == 9)
                            SS = val;//int.Parse(cellValue);
                        else if (j == 10)
                            computer = val;//int.Parse(cellValue);
                        else if (j == 11)
                            arts = val;//int.Parse(cellValue);
                        else if (j == 12)
                            conduct = val;//int.Parse(cellValue);
                        else if (j == 13)
                        {
                            attendance = val;// int.Parse(cellValue);
                        }
                        else if (j == 14)
                        {
                            result_date = cellValue;
                            var t = result_date.Split("/");
                            result_date = t[1] + "-" + t[0] + "-" + t[2];
                        }
                        
                    }
                    if (id < 1)
                    { break; }
                    Models.Subject sub =
                        new Models.Subject
                        {
                            Student_Roll = id,
                            Quran = quran,
                            Arabic = arabic,
                            Maths = maths,
                            English = english,
                            Science = Sci,
                            Social = SS,
                            Computer = computer,
                            Conduct = conduct,
                            Arts = arts,
                            Attendance = attendance,
                            Islamic_Studies = Isl,
                            Class = className,
                            Result_Date = result_date,
                        };
                    var c = sub.Class;
                    if (c.Contains("-"))
                    { }
                    else
                    {
                        var c_temp = c.Split(" ");
                        c = c_temp[0] + "-" + c_temp[1] + " " + c_temp[2];
                    }
                    var student = _db.Students.FirstOrDefault(x => x.Roll == sub.Student_Roll && (x.Grade_Name == sub.Class || x.Grade_Name == c) && x.IsActive == "Yes");
                    sub.Class = c;
                    if (student != null)
                    {
                        
                        var s = _db.Subjects.Where(x => x.Student_Roll == sub.Student_Roll).ToList();
                        if(s.Any())
                        {
                            var temp = s.FirstOrDefault(x => x.Semester == 1 && x.Class == sub.Class);
                            
                           
                            if(temp==null)
                            {
                                sub.Semester = 1;
                                sub.IsActive = "Yes";
                                _db.Subjects.Add(sub);
                                _db.SaveChanges();
                            }
                            else
                            {
                                sub.Semester = 1;
                                sub.IsActive = "Yes";
                                _db.Subjects.Remove(temp);
                                _db.Subjects.Update(sub);
                                _db.SaveChanges();
                            }
                        }
                        else
                        {
                            sub.Semester = 1;
                            sub.IsActive = "Yes";
                            _db.Subjects.Add(sub);
                            _db.SaveChanges();
                        }
                        
                    }
                }
            }
        }

        public void read_excel1()
        {
            string filep = _hostingEnvironment.WebRootPath;
            string filepath = filep + "/files/Semester2.xlsx";
            IWorkbook workbook;
            FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            workbook = new XSSFWorkbook(fs);
            ISheet sheet = workbook.GetSheetAt(0);
            if (sheet != null)
            {

                int rowCount = sheet.LastRowNum; // This may not be valid row count.
                                                 // If first row is table head, i starts from 1
                for (int i = 1; i <= rowCount; i++)
                {
                    IRow curRow = sheet.GetRow(i);
                    // Works for consecutive data. Use continue otherwise 
                    if (curRow == null)
                    {
                        // Valid row count
                        rowCount = i - 1;
                        break;
                    }
                    int quran = 0, english = 0, Sci = 0, Isl = 0, arabic = 0, maths = 0, conduct = 0, attendance = 0, computer = 0, SS = 0, arts = 0;
                    int id = 0;
                    string className = null, result_date = null;
                    // Get data from the 4th column (4th cell of each row)
                    DataFormatter formatter = new DataFormatter();
                    for (int j = 0; j <= 14; j++)
                    {


                        var cell_type = curRow.GetType();
                        string cellValue = null;
                        if (j == 14)
                        {
                            cellValue = curRow.GetCell(j).DateCellValue.ToString();
                            cellValue = cellValue.Remove(cellValue.Length - 12);
                        }
                        else
                        {
                            if (curRow.GetCell(j).CellType.ToString() == "Numeric")
                            {
                                cellValue = curRow.GetCell(j).NumericCellValue.ToString();
                            }
                            else
                                cellValue = curRow.GetCell(j).StringCellValue;
                        }
                        int val = 0;
                        if (j != 0 && j != 1 && j != 2 && j != 14)
                        {
                            if (cellValue.Contains("."))
                            {
                                var input = cellValue.Substring(0, cellValue.IndexOf(".") + 1);
                                input = input.Replace(".", "");
                                val = int.Parse(input);
                            }
                            else
                                val = int.Parse(cellValue);
                        }

                        if (j == 0)
                            id = int.Parse(cellValue);
                        else if (j == 1)
                            className = cellValue;
                        else if (j == 3)
                            quran = val;// int.Parse(cellValue);
                        else if (j == 4)
                            Isl = val;// int.Parse(cellValue);
                        else if (j == 5)
                            arabic = val;// int.Parse(cellValue);
                        else if (j == 6)
                            english = val;//int.Parse(cellValue);
                        else if (j == 7)
                            maths = val;//int.Parse(cellValue);
                        else if (j == 8)
                            Sci = val;//int.Parse(cellValue);
                        else if (j == 9)
                            SS = val;//int.Parse(cellValue);
                        else if (j == 10)
                            computer = val;//int.Parse(cellValue);
                        else if (j == 11)
                            arts = val;//int.Parse(cellValue);
                        else if (j == 12)
                            conduct = val;//int.Parse(cellValue);
                        else if (j == 13)
                        {
                            attendance = val;// int.Parse(cellValue);
                        }
                        else if (j == 14)
                        {
                            result_date = cellValue;
                            var t = result_date.Split("/");
                            result_date = t[1] + "-" + t[0] + "-" + t[2];
                        }

                    }
                    if (id < 1)
                    { break; }
                    Models.Subject sub =
                        new Models.Subject
                        {
                            Student_Roll = id,
                            Quran = quran,
                            Arabic = arabic,
                            Maths = maths,
                            English = english,
                            Science = Sci,
                            Social = SS,
                            Computer = computer,
                            Conduct = conduct,
                            Arts = arts,
                            Attendance = attendance,
                            Islamic_Studies = Isl,
                            Class = className,
                            Result_Date = result_date,
                        };
                    var c = sub.Class;
                    if (c.Contains("-"))
                    { }
                    else
                    {
                        var c_temp = c.Split(" ");
                        c = c_temp[0] + "-" + c_temp[1] + " " + c_temp[2];
                    }
                    var student = _db.Students.FirstOrDefault(x => x.Roll == sub.Student_Roll && (x.Grade_Name == sub.Class || x.Grade_Name == c) && x.IsActive == "Yes");
                    sub.Class = c;
                    if (student != null)
                    {

                        var s = _db.Subjects.Where(x => x.Student_Roll == sub.Student_Roll).ToList();
                        if (s.Any())
                        {
                            
                            var temp = s.FirstOrDefault(x => x.Semester == 2 && x.Class == sub.Class);
                            

                            if (temp == null)
                            {
                               sub.Semester = 2;
                               sub.IsActive = "Yes";
                               _db.Subjects.Add(sub);
                               _db.SaveChanges();
                            }
                            else
                            {
                                sub.Semester = 2;
                                sub.IsActive = "Yes";
                                _db.Subjects.Remove(temp);
                                _db.Subjects.Update(sub);
                                _db.SaveChanges();
                            }
                        }
                        else
                        {
                            sub.Semester = 2;
                            sub.IsActive = "Yes";
                            _db.Subjects.Add(sub);
                            _db.SaveChanges();
                        }

                    }
                }
            }
        }


    }
}